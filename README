             _     
  __ _  ___ | |__  
 / _` |/ __|| '_ \ 
| (_| |\__ \| | | |
 \__,_||___/|_| |_|
ash R toolbox


README
	Ash is a toolbox of R code that perfoms a stationarity
	analysis of the low water regime which in french is "analyse
	de stationnarité du régime des étiages". In this
	configuration, this analysis is centered on the Adour-Garonne
	hydrological basin which is located in the south-west part of
	France.

	The plan of a first use is developed below :

	1. In order to manage to use this code, you firstly need to
	install R (more info here : https://www.r-project.org) but
	also for R studio if you want a user-friendly text editor
	(more info here : https://www.rstudio.com/).

	2. Then if you fill confident you can directly jump to part 3.
	Otherwise, open the 'script_install.R' file. This file guide
	you throught the different package you need to install to make
	this toolbox functionnal. You can run this entire code
	(RStudio : Ctrl+Alt+R).

	3. Open the 'main.R' file. This script is your script of
	interaction with the rest of the code. You have to put here
	your settings and execute the entire file when you are ready.
	(RStudio : Ctrl+Alt+R) It is possible that some packages will
	be missing. Take the time to install them before re-execute
	the 'main.R' file.

	4. Figures will be stored in the 'figures' repository and
	results in the 'results' repository. All the rest of the code
	is located in the 'Rcode' repository and is divided between
	'processing' and 'plotting'. The 'data' repository is where
	you can add, remove or modify data you want to analyse and the
	'resources' repository is for fixed 'resources' that the code
	needed.


CONTACT
	If you have problems, questions, ideas or suggestions, please
	contact us. Contact first Louis Héraut who is the main
	developer. If it is not possible, Éric Sauquet is the main
	referent at INRAE to contact :
	
	Louis Héraut : <louis.heraut@inrae.fr>
	Éric Sauquet : <eric.sauquet@inrae.fr>


GIT
	To download the very latest source off the GIT server do this:
	git clone https://gitlab.irstea.fr/louis.heraut/ash.git

	(you will get a directory named ash created, filled with the
	source code)


NOTICE
	Feel free to use all the code or only some parts but it would
	be nice to at least mention the name of the authors.
	
	Ash stand for "Analyse de Stationnarité Hydrologique".
